<?php include 'parts/head.php'; 
include 'controllers/dateCtrl.php';
?>
<!--afficher toutes les informations d'un rendez-vous
permettre la modification de ce rendez-vous-->
<div id="infoDate">
        <?php if ($isDateFound) { ?>
        <h1 class="text-center">Rendez-vous de <?= $date->getLastname() . ' ' . $date->getFirstname() ;?></h1>
        <div class="container formNana text-center" action="" method="POST">
                <label class="row col-2 pt-3 mx-auto" for="listPatient" class="form-label">Nom et prénom du patient : </label>
                <p><?= $date->getLastname() . ' ' . $date->getFirstname() ?></p>
                <label class="row col-2 pt-3 mx-auto" for="dateTime" class="form-label">Jour et heure du rendez-vous : </label>
                <p><?= $date->getDateHour() ?></p>
                <button id="clickInfoDate" class="aNana m-5 text-center mx-auto d-block col-2">Modifier les informations</button>
        </div>
        <?php } else { ?>
        <h1 class="text-center">Rendez-vous non trouvé</h1>
        <p class="text-danger text-center fs-4">Merci de contacter le service technique si le problème persiste!</p>
        <a class="aNana row col-1 m-5 text-center mx-auto d-block" href="liste-patients.php" class="btn btn-primary">Retour</a>
        <?php } ?>
</div>
<div id="formDateModif">
        <h1 class="text-center">Modifier rendez-vous</h1>
        <form class="container formNana text-center" action="" method="POST">
                <label class="row col-2 pt-3 mx-auto" for="listPatient" class="form-label">Nom et prénom du patient : </label>
                <select class="row col-2 mx-auto" list="patient" id="listPatient" name="listPatient">
                        <option class="optionDatalist" value="<?= $date->getIdPatients() ?>">
                        <?= $date->getLastname() . ' ' . $date->getFirstname();?>
                        </option>
                </select>
                <?php if (!empty($errorList['listPatient'])) { ?>
                        <p class="text-danger"><?= $errorList['listPatient']; ?></p>
                <?php } ?>
                <label class="row col-2 pt-3 mx-auto" for="dateTime" class="form-label">Jour et heure du rendez-vous : </label>
                <input class="col-2" type="datetime-local" name="dateTime">
                <?php if (!empty($errorList['dateTime'])) { ?>
                        <p class="text-danger"><?= $errorList['dateTime']; ?></p>
                <?php } ?>
                <input class="row col-2 mt-5 mx-auto btnNana" type="submit" value="Enregistrer les modifications"name="modifDate">
                <?php if (!empty($errorList['addDate'])) { ?>
                        <p class="text-danger"><?= $errorList['addDate']; ?></p>
                <?php } ?>
        </form>
</div>
<?php include 'parts/footer.php'; ?>