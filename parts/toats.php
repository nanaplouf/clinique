<div class="toast" role="alert" aria-live="assertive" aria-atomic="true">
  <div class="toast-header">
    <strong class="me-auto"><?= isset($pageTitle) ? $pageTitle : 'Hopital Shélémors'?></strong>
    <small><?= date("d/m/Y - H:i")?></small>
    <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
  </div>
  <div class="toast-body">
    Le patient a bien été enregistré.
  </div>
</div>