<!DOCTYPE html>
<html lang="fr" dir="ltr">
<head>
    <meta charset="utf-8">
    <title><?= isset($pageTitle) ? $pageTitle : 'Hopital Shélémors' ?></title>
    <link rel="icon" type="image/png" href="../assets/img/logoShele.png">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="../assets/css/main.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
</head>
<body>
<nav class="container-fluid navNana">
    <div class="row">
        <div class="col-1">
            <img src="../assets/img/logoShele.png" alt="logo Shelémors">
        </div>
        <div class="col-2 text-start">
            <a href="index.php" class="aNana text-start"><i class="fas fa-user-md"></i> Acceuil</a>
        </div>
        <div class="col text-end">
            <a href="addPatient.php" class="aNana"><i class="fas fa-user-plus"></i> Ajouter un patient</a>
            <a href="listPatient.php" class="aNana"><i class="fas fa-users"></i> Liste patient</a>
            <a href="addDate.php" class="aNana"><i class="fas fa-calendar-alt"></i> Ajouter un rendez-vous</a>
            <a href="listDate.php" class="aNana"><i class="far fa-list-alt"></i> Liste des rendez-vous</a>
            <a href="addDatePatient.php" class="aNana">Ajouter un RDV et patient</a>
        </div>
    </div>
</nav>
<div class="bodyNana">