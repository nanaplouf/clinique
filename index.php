<?php include 'parts/head.php';?>
<h1 class="text-center">Bienvenue</h1>
<div class="container-fluid row">
    <div class="col-6 m-5 mx-auto d-block">
    <img src="assets/img/welcom.jpeg" class="opacity-75 rounded" alt="Image de mains humaines avec des logo de medecine blanc." height="350px">
    </div>
    <div class="col m-5 text-center">
        <h2 class="m-5">Clinique Shélémors</h2>
        <p class="textIndex">On a beau avoir une santé de fer, on finit toujours par rouiller.</p>
    </div>
</div>
<?php include 'parts/footer.php';?>