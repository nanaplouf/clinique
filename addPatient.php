<?php 
include 'parts/head.php'; 
include 'controllers/addPatientCtrl.php';
?>
<!-- il un formulaire pour creer un nouveau patient-->
<h1 class="text-center">Nouveau patient</h1>
<form class="container formNana text-center" action="" method="POST">
    <?php foreach ($inputArray as $input) { ?>
        <label class="row col-2 pt-3 mx-auto" for="<?= $input['name']?>" class="form-label"><?= $input['label']?></label>
        <input class="col-2" type="<?= $input['type']?>" class="form-control" id="<?= $input['name']?>" name="<?= $input['name']?>" placeholder="<?= $input['placeholder']?>">
        <?php if (!empty($errorList[$input['name']])) { ?><p class="text-danger"><?= $errorList[$input['name']] ?></p><?php } 
         } ?>
        <input class="row col-2 mt-5 mx-auto btnNana" type="submit" name="addPatient" value="Ajout du patient"name="validation" value="Enregistrer">
</form>
<?php include 'parts/footer.php';?>
