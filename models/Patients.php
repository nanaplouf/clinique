<?php
require_once 'AbstractModel.php';
class Patients extends AbstractModel
{
    private int $id;
    private string $lastname;
    private string $firstname;
    private string $birthdate;
    private string $birthdateView;
    private string $phone;
    private string $mail;
    private string $idPatients;
    private string $date;
    private string $hour;
    private string $table = '`patients`';


    public function modifPatient(): bool
    {
        $query = 'UPDATE ' . $this->table . ' SET `lastname` = :lastname, `firstname` = :firstname, `birthdate` = :birthdate, `phone` = :phone, `mail`= :mail 
        WHERE `id` = :id';
        $queryStatement = $this->db->prepare($query);
        $queryStatement->bindValue(':id', $this->id, PDO::PARAM_INT);
        $queryStatement->bindValue(':lastname', $this->lastname, PDO::PARAM_STR);
        $queryStatement->bindValue(':firstname', $this->firstname, PDO::PARAM_STR);
        $queryStatement->bindValue(':birthdate', $this->birthdate, PDO::PARAM_STR);
        $queryStatement->bindValue(':phone', $this->phone, PDO::PARAM_STR);
        $queryStatement->bindValue(':mail', $this->mail, PDO::PARAM_STR);
        return $queryStatement->execute();
    }
    public function addPatient(): bool
    {
        $query = 'INSERT INTO ' . $this->table
            . ' (`lastname`,`firstname`,`birthdate`,`phone`,`mail`) '
            . 'VALUES (:lastname, :firstname, :birthdate, :phone, :mail)';
        $queryStatement = $this->db->prepare($query);
        $queryStatement->bindValue(':lastname', $this->lastname, PDO::PARAM_STR);
        $queryStatement->bindValue(':firstname', $this->firstname, PDO::PARAM_STR);
        $queryStatement->bindValue(':birthdate', $this->birthdate, PDO::PARAM_STR);
        $queryStatement->bindValue(':phone', $this->phone, PDO::PARAM_STR);
        $queryStatement->bindValue(':mail', $this->mail, PDO::PARAM_STR);
        return $queryStatement->execute();
    }
    /**
     * Permet de savoir si un patient est unique
     *
     * @return boolean
     */
    public function checkPatientIfExists(): bool
    {
        $check = false;
        $query = 'SELECT COUNT(`id`) AS `number` FROM ' . $this->table
            . ' WHERE `lastname` = :lastname AND `firstname` = :firstname AND `birthdate` = :birthdate';
        $queryStatement = $this->db->prepare($query);
        $queryStatement->bindValue(':lastname', $this->lastname, PDO::PARAM_STR);
        $queryStatement->bindValue(':firstname', $this->firstname, PDO::PARAM_STR);
        $queryStatement->bindValue(':birthdate', $this->birthdate, PDO::PARAM_STR);
        $queryStatement->execute();
        // $number = $queryStatement->fetch(PDO::FETCH_OBJ)->number;
        $toto = $queryStatement->fetch(PDO::FETCH_OBJ);
        // number = 0 si il n'y a pas de patient identique
        // number = 1 si il y a un patient identique
        $number = $toto->number;
        if ($number) {
            $check = true;
        }
        return $check;
    }
    public function getPatientsList(): array
    {
        $query = 'SELECT `id`,`lastname`, `firstname`, DATE_FORMAT(`birthdate`, \'%d/%m/%Y\') AS `birthdate` FROM ' . $this->table;
        $queryStatement = $this->db->query($query);
        return $queryStatement->fetchAll(PDO::FETCH_OBJ);
    }
    public function getPatientInfo(): bool
    {
        $query = 'SELECT `lastname`, `firstname`, 
        DATE_FORMAT(`birthdate`, \'%d/%m/%Y\') AS `birthdate`, 
        `birthdate` AS `birthdateView`, `phone`, `mail` FROM ' . $this->table
            . ' WHERE id= :id';
        $queryStatement = $this->db->prepare($query);
        $queryStatement->bindValue(':id', $this->id, PDO::PARAM_INT);
        $queryStatement->execute();
        $result = $queryStatement->fetch(PDO::FETCH_OBJ);
        //Si j'ai un résultat j'hydrate mon objet.
        if(is_object($result)){
            $this->lastname = $result->lastname;
            $this->firstname = $result->firstname;
            $this->birthdate = $result->birthdate;
            $this->birthdateView = $result->birthdateView;
            $this->phone = $result->phone;
            $this->mail = $result->mail;
            return true;
        }
        return false;
    }
    public function getPatientListSelect(?string $search = null): array
    {
        $query = 'SELECT `id`, `lastname`, `firstname`, DATE_FORMAT(`birthdate`,\'%d/%m/%Y\') AS `birthdate`
        FROM ' . $this->table;
        if (!is_null($search)) {
            $query .= 'WHERE `lastname` LIKE :search OR `firstname` LIKE :search OR `birthdate` LIKE :search';
            $queryStatement = $this->db->prepare($query);
            $queryStatement->bindValue(':search', '%' . $search . '%', PDO::PARAM_STR);
            $queryStatement->execute();
        } else {
            $queryStatement = $this->db->query($query);
        }
        return $queryStatement->fetchAll(PDO::FETCH_OBJ);
    }

    public function getPatientDates()
    {
        $query = 'SELECT `patients`.`id`, `appointments`.`idPatients`, 
        DATE_FORMAT(`appointments`.`dateHour`, \'%d/%m/%Y\') AS `date`, 
        DATE_FORMAT(`appointments`.`dateHour`, \'%H:%i\') AS `hour`
        FROM `patients`
        LEFT JOIN `appointments` 
            ON `patients`.`id` = `appointments`.`idPatients`
        WHERE `patients`.`id`= :id';
        $queryStatement = $this->db->prepare($query);
        $queryStatement->bindValue(':id', $this->id, PDO::PARAM_INT);
        $queryStatement->execute();
        return $queryStatement->fetchAll(PDO::FETCH_OBJ);
    }
    public function countPatient()
    {
        $query = 'SELECT COUNT(`id`) AS nb_articles 
        FROM ' . $this->table;
        $queryStatement = $this->db->query($query);
        return $queryStatement->fetch(PDO::FETCH_OBJ);
    }
    
    /**
     * @param int         $premier
     * @param int         $parPage
     * @param string|null $search
     *
     * @return array|false
     */
    public function pagination(int $premier, int $parPage, ?string $search = null)
    {
        $query = 'SELECT `id`, `lastname`, `firstname`, DATE_FORMAT(`birthdate`,\'%d/%m/%Y\') AS `birthdate` FROM ' .
            $this->table;
        if (!is_null($search)) {
            $query .= 'WHERE `lastname` LIKE :search OR `firstname` LIKE :search OR `birthdate` LIKE :search';
        }
        $query .=  'ORDER BY `id` DESC LIMIT :premier, :parpage;';
        $queryStatement = $this->db->prepare($query);
        if (!is_null($search)) {
            $queryStatement->bindValue(':search', '%' . $search . '%', PDO::PARAM_STR);
        }
        $queryStatement->bindValue(':premier', $premier, PDO::PARAM_INT);
        $queryStatement->bindValue(':parpage', $parPage, PDO::PARAM_INT);
        $queryStatement->execute();
        return $queryStatement->fetchAll(PDO::FETCH_OBJ);
    }

    public function deletePatient()
    {
        $query = 'DELETE FROM `patients` WHERE `id` = :id';
        $queryStatement = $this->db->prepare($query);
        $queryStatement->bindValue(':id', $this->id, PDO::PARAM_INT);
        return $queryStatement->execute();
    }

    /***
     * SETTER
     */
    public function setId(int $value): void
    {
        $this->id = $value;
    }
    public function setLastname(string $value): void
    {
        $this->lastname = strtoupper($value);
    }

    public function setFirstname(string $value): void
    {
        $this->firstname = $value;
    }

    public function setBirthdate(string $value): void
    {
        $this->birthdate = $value;
    }

    public function setBirthdateView(string $value): void
    {
        $this->birthdateView = $value;
    }

    public function setPhone(string $value): void
    {
        $value = str_replace([' ', '.', '-'], '', $value);
        $this->phone = $value;
    }

    public function setMail(string $value): void
    {
        $this->mail = $value;
    }

    /***
     * GETTER
     */
    public function getId():int{
        return $this->id;
    }

    public function getLastname():string{
        return $this->lastname;
    }

    public function getFirstname():string{
        return $this->firstname;
    }

    public function getBirthdate():string{
        return $this->birthdate;
    }

    public function getBirthdateView():string{
        return $this->birthdateView;
    }

    public function getPhone():string{
        return $this->phone;
    }

    public function getMail():string{
        return $this->mail;
    }

    public function getIdPatients():string{
        return $this->idPatients;
    }
    public function getDate():string{
        return $this->date;
    }
    public function getHour():string{
        return $this->hour;
    }
}
