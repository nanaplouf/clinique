<?php
require_once 'AbstractModel.php';
class PatientDate extends AbstractModel
{
    private int $dateId;
    private string $lastname;
    private string $firstname;
    private string $birthdate;
    private string $dateHour;
    private int $idPatients;
    private string $phone;
    private string $mail;
    private int $id;
    private string $date;
    private string $hour;
    private string $tableP = '`patients`';
    private string $tableD = '`appointments`';

    public function addPatientDate(): bool
    {
        $query = 'INSERT INTO ' . $this->tableP
            . ' (`lastname`,`firstname`,`birthdate`,`phone`,`mail`) '
            . 'VALUES (:lastname, :firstname, :birthdate, :phone, :mail)';
        $query = 'INSERT INTO ' . $this->tableD
            . ' (`dateHour`,`idPatients`)'
            . 'VALUES (:dateHour, :idPatients)';
        $queryStatement = $this->db->prepare($query);
        $queryStatement->bindValue(':lastname', $this->lastname, PDO::PARAM_STR);
        $queryStatement->bindValue(':firstname', $this->firstname, PDO::PARAM_STR);
        $queryStatement->bindValue(':birthdate', $this->birthdate, PDO::PARAM_STR);
        $queryStatement->bindValue(':phone', $this->phone, PDO::PARAM_STR);
        $queryStatement->bindValue(':mail', $this->mail, PDO::PARAM_STR);
        $queryStatement->bindValue(':dateHour', $this->dateHour, PDO::PARAM_STR);

        return $queryStatement->execute();
    }
    public function checkPatientIfExists(): bool
    {
        $check = false;
        $query = 'SELECT COUNT(`id`) AS `number` FROM ' . $this->tableP
            . ' WHERE `lastname` = :lastname AND `firstname` = :firstname AND `birthdate` = :birthdate';
        $queryStatement = $this->db->prepare($query);
        $queryStatement->bindValue(':lastname', $this->lastname, PDO::PARAM_STR);
        $queryStatement->bindValue(':firstname', $this->firstname, PDO::PARAM_STR);
        $queryStatement->bindValue(':birthdate', $this->birthdate, PDO::PARAM_STR);
        $queryStatement->execute();
        // $number = $queryStatement->fetch(PDO::FETCH_OBJ)->number;
        $toto = $queryStatement->fetch(PDO::FETCH_OBJ);
        // number = 0 si il n'y a pas de patient identique
        // number = 1 si il y a un patient identique
        $number = $toto->number;
        if ($number) {
            $check = true;
        }
        return $check;
    }

    /***
     * SETTER
     */
    public function setDateId(int $value): void
    {
        $this->dateId = $value;
    }
    public function setDateHour(string $value): void
    {
        $this->dateHour = $value;
    }
    public function setIdPatients(string $value): void
    {
        $this->idPatients = $value;
    }
    public function setBirthdateView(string $value): void
    {
        $this->birthdateView = $value;
    }

    public function setPhone(string $value): void
    {
        $value = str_replace([' ', '.', '-'], '', $value);
        $this->phone = $value;
    }

    public function setMail(string $value): void
    {
        $this->mail = $value;
    }
    public function setLastname(string $value): void
    {
        $this->lastname = strtoupper($value);
    }

    public function setFirstname(string $value): void
    {
        $this->firstname = $value;
    }

    public function setBirthdate(string $value): void
    {
        $this->birthdate = $value;
    }


    /***
     * GETTER
     */
    public function getId():int{
        return $this->id;
    }

    public function getBirthdateView():string{
        return $this->birthdateView;
    }

    public function getPhone():string{
        return $this->phone;
    }

    public function getMail():string{
        return $this->mail;
    }
    public function getDate():string{
        return $this->date;
    }
    public function getHour():string{
        return $this->hour;
    }
    public function getDateId():int
    {
        return $this->dateId;
    }
    public function getDateHour():string
    {
        return $this->dateHour;
    }
    public function getIdPatients():int
    {
        return $this->idPatients;
    }
    public function getLastname():string
    {
        return $this->lastname;
    }
    public function getFirstname():string
    {
        return $this->firstname;
    }
    public function getBirthdate():string
    {
        return $this->birthdate;
    }
}
