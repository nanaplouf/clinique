<?php

abstract class AbstractModel
{
    /** @var PDO $db */
    protected PDO $db;

    /** @var string $host */
    private string $host;

    /** @var string $name */
    private string $name;

    /** @var string $charset */
    private string $charset;

    /** @var string $user */
    private string $user;

    /** @var string $password  */
    private string $password;

    /**
     * AbstractModel constructor
     */
    public function __construct()
    {
        try {
            $this->host = $this->getenv_docker('DB_HOST', 'localhost');
            $this->name = $this->getenv_docker('DB_NAME', 'hospitale2n');
            $this->charset = $this->getenv_docker('DB_CHARSET', 'utf8');
            $this->user = $this->getenv_docker('DB_USER', 'root');
            $this->password = $this->getenv_docker('DB_PASSWORD', '');

            $dsn = implode('', [
                'mysql:host=',
                $this->host,
                ';dbname=',
                $this->name,
                ';charset=',
                $this->charset
            ]);
            $this->db = new PDO($dsn, $this->user, $this->password);
        } catch (Exception $error) {
            die($error->getMessage());
        }
    }

    /**
     * @param $env
     * @param $default
     *
     * @return array|false|mixed|string
     */
    protected function getenv_docker(string $env, string $default): string
    {
        if ($fileEnv = getenv($env . '_FILE')) {
            return rtrim(file_get_contents($fileEnv), "\r\n");
        }
        else if (($val = getenv($env)) !== false) {
            return $val;
        }
        else {
            return $default;
        }
    }
}
