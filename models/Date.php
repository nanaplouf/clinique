<?php
require_once 'AbstractModel.php';
class Date extends AbstractModel
{
    private int $dateId;
    private string $lastname;
    private string $firstname;
    private string $birthdate;
    private string $dateHour;
    private int $idPatients;
    private string $table = '`appointments`';

    public function addDate(): bool
    {
        $query = 'INSERT INTO ' . $this->table
            . ' (`dateHour`,`idPatients`)'
            . 'VALUES (:dateHour, :idPatients)';
        $queryStatement = $this->db->prepare($query);
        $queryStatement->bindValue(':dateHour', $this->dateHour, PDO::PARAM_STR);
        $queryStatement->bindValue(':idPatients', $this->idPatients, PDO::PARAM_STR);

        return $queryStatement->execute();
    }

    public function modifDate(): bool
    {
        $query = 'UPDATE ' . $this->table . ' SET `dateHour` = :dateHour, `idPatients` = :idPatients
        WHERE`id` = :id';
        $queryStatement = $this->db->prepare($query);
        $queryStatement->bindValue(':id', $this->dateId, PDO::PARAM_INT);
        $queryStatement->bindValue(':dateHour', $this->dateHour, PDO::PARAM_STR);
        $queryStatement->bindValue(':idPatients', $this->idPatients, PDO::PARAM_STR);

        //return $queryStatement->execute();
        $result = $queryStatement->execute();
        var_dump($result);
        if(!$result) {
        var_dump($queryStatement->errorInfo());
        }
        return $result;
    }

     /**
     * Permet de savoir si une heure est unique
     *
     * @return boolean
     */
    public function checkHourIfExists(): bool
    {
        $check = false;
        $query = 'SELECT COUNT(`id`) AS `number` FROM ' . $this->table
            . ' WHERE `dateHour` = :dateHour';
        $queryStatement = $this->db->prepare($query);
        $queryStatement->bindValue(':dateHour', $this->dateHour, PDO::PARAM_STR);
        $queryStatement->execute();
        // $number = $queryStatement->fetch(PDO::FETCH_OBJ)->number;
        $toto = $queryStatement->fetch(PDO::FETCH_OBJ);
        // number = 0 si il n'y a pas de patient identique
        // number = 1 si il y a un patient identique
        $number = $toto->number;
        if ($number) {
            $check = true;
        }
        return $check;
    }

    public function getDatesList(): array
    {
        $query = 'SELECT `appointments`.`id` AS `dateId`, `patients`.`firstname`, `patients`.`lastname`, `patients`.`id`, `dateHour`, `idPatients`, DATE_FORMAT(`dateHour`, \'%d/%m/%Y\') AS `date`, 
        DATE_FORMAT(`dateHour`, \'%H:%i\') AS `hour`
        FROM `appointments`
        LEFT JOIN `patients` ON `appointments`.`idPatients` = `patients`.`id`
        ORDER BY `date`, `hour` ASC';
        $queryStatement = $this->db->query($query);
        return $queryStatement->fetchAll(PDO::FETCH_OBJ);
    }
    public function getDatesInfo(): bool
    {
        $query = 'SELECT `appointments`.`id` AS `dateId`, `idPatients`, DATE_FORMAT(`dateHour`, \'%d/%m/%Y - %H:%i\') AS `dateHour`,  
        `patients`.`firstname`, `patients`.`lastname`, 
        DATE_FORMAT(`patients`.`birthdate`, \'%d/%m/%Y\') AS `birthdate`, `patients`.`id` 
        FROM `appointments`
        LEFT JOIN `patients` ON `appointments`.`idPatients` = `patients`.`id`
        WHERE `appointments`.`id` = :id';
        $queryStatement = $this->db->prepare($query);
        $queryStatement->bindValue(':id', $this->dateId, PDO::PARAM_INT);
        $queryStatement->execute();
        $result = $queryStatement->fetch(PDO::FETCH_OBJ);
        //Si j'ai un résultat j'hydrate mon objet. 
        if(is_object($result)){
            $this->lastname = $result->lastname;
            $this->firstname = $result->firstname;
            $this->dateHour = $result->dateHour;
            $this->birthdate = $result->birthdate;
            $this->idPatients = $result->idPatients;
            return true;
        }
        return false;
    }

    public function deleteDate()
    {
        $query = 'DELETE FROM `appointments`
        WHERE `id` = :id';
         $queryStatement = $this->db->prepare($query);
         $queryStatement->bindValue(':id', $this->dateId, PDO::PARAM_INT);
         return $queryStatement->execute(); 

    }
    /***
     * SETTER
     */
    public function setDateId(int $value): void
    {
        $this->dateId = $value;
    }
    public function setDateHour(string $value): void
    {
        $this->dateHour = $value;
    }
    public function setIdPatients(string $value): void
    {
        $this->idPatients = $value;
    }
    public function setLastname(string $value): void
    {
        $this->lastname = $value;
    }
    public function setFirstname(string $value): void
    {
        $this->firstname = $value;
    }
    public function setBirthdate(string $value): void
    {
        $this->birthdate = $value;
    }

    /***
     * GETTER
     */
    public function getDateId():int
    {
        return $this->dateId;
    }
    public function getDateHour():string
    {
        return $this->dateHour;
    }
    public function getIdPatients():int
    {
        return $this->idPatients;
    }
    public function getLastname():string
    {
        return $this->lastname;
    }
    public function getFirstname():string
    {
        return $this->firstname;
    }
    public function getBirthdate():string
    {
        return $this->birthdate;
    }
}
