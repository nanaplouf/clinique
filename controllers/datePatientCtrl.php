<?php
require 'class/DatePatientForm.php';
require 'models/PatientDate.php';

$inputArray = [
    ['filter' => 'name', 'name' => 'firstname', 'realName' => 'un prénom', 'placeholder' => '', 'label' => 'Prénom', 'type' => 'text'],
    ['filter' => 'name', 'name' => 'lastname', 'realName' => 'un nom de famille', 'placeholder' => '', 'label' => 'Nom de famille', 'type' => 'text'],
    ['filter' => 'date', 'name' => 'birthdate', 'realName' => 'une date de naissance', 'placeholder' => '', 'label' => 'Date de naissance', 'type' => 'date'],
    ['filter' => 'phone', 'name' => 'phone', 'realName' => 'un numéro de téléphone', 'placeholder' => '', 'label' => 'Téléphone', 'type' => 'text'],
    ['filter' => 'email', 'name' => 'mail', 'realName' => 'une adresse de couriel', 'placeholder' => '', 'label' => 'Adresse de courriel', 'type' => 'email'],
    ['filter' => 'dateTime', 'name' => 'dateTime', 'realName' => 'la date et l\'heure', 'placeholder' => '', 'label' => 'Jour et heure du rendez-vous', 'type' => 'datetime-local'],
];
//Quand l'utilisateur a appuyé sur le bouton
if (isset($_POST['validation'])) {
    $errorList = [];
    $formVerif = new DatePatientForm;

    $valueArray = [];
    foreach ($inputArray  as $input) {
        if ($formVerif->checkPost($input)) {
            $valueArray[$input['name']] = $_POST[$input['name']];
        } else {
            $errorList[$input['name']] = $formVerif->getErrorMessage();
        }
    }

    if (count($errorList) == 0) {
        $patientDate = new PatientDate;
        $patientDate->setLastname(htmlspecialchars($valueArray['lastname']));
        $patientDate->setFirstname(htmlspecialchars($valueArray['firstname']));
        $patientDate->setBirthdate(htmlspecialchars($valueArray['birthdate']));
        $patientDate->setPhone(htmlspecialchars($valueArray['phone']));
        $patientDate->setMail(htmlspecialchars($valueArray['mail']));
        $patientDate->setDateHour(htmlspecialchars($valueArray['dateTime']));
        if (!$patientDate->checkPatientIfExists()) {
//            $patientDate->addPatientDate();
            header('location: index.php');
            exit;
        }else{
            $errorList['addPatientDate'] = 'Ce patient existe déjà';
        }
    }
}
