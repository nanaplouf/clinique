<?php
require 'class/DateForm.php';
require 'models/Date.php';
//Quand l'utilisateur a appuyé sur le bouton
$inputArray = [
    ['filter' => 'listPatient', 'name' => 'listPatient', 'realName' => 'l\'id', 'placeholder' => '', 'label' => 'Recherche nom du patient', 'type' => 'text'],
    ['filter' => 'dateTime', 'name' => 'dateTime', 'realName' => 'la date et l\'heure', 'placeholder' => '', 'label' => 'Jour et heure du rendez-vous', 'type' => 'datetime-local']
];
if (isset($_POST['validation'])) {
    $errorList = [];
    $formVerif = new DateForm;

    $valueArray = [];
    foreach ($inputArray as $input) {
        if ($formVerif->checkPost($input)) {
            $valueArray[$input['name']] = $_POST[$input['name']];
        } else {
            $errorList[$input['name']] = $formVerif->getErrorMessage();
        }
    }
    if (count($errorList) == 0) {
        $date = new Date;
        $date->setIdPatients(htmlspecialchars($valueArray['listPatient']));
        $date->setDateHour(htmlspecialchars($valueArray['dateTime']));
        if (!$date->checkHourIfExists()) {
//            $date->addDate();
            //include 'parts/toats.php';
            header('location: index.php');
            exit;
        }else{
            $errorList['addDate'] = 'Ce patient a déjà un rendez-vous!';
        }
    }
}

