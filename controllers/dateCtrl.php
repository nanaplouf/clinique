<?php
require 'class/DateForm.php';
require 'models/Date.php';
$scriptList = ['assets/js/formDate.js'];
$isDateFound = false;
if (isset($_GET['dateId'])) {
    $date = new Date;
    $date->setDateId(htmlspecialchars($_GET['dateId']));
    $isDateFound = $date->getDatesInfo();
}
$inputArray = [
    ['filter' => 'listPatient', 'name' => 'listPatient', 'realName' => 'l\'id', 'placeholder' => '', 'label' => 'Recherche nom du patient', 'type' => 'text', 'value'=> $date->getIdPatients()],
    ['filter' => 'dateTime', 'name' => 'dateTime', 'realName' => 'la date et l\'heure', 'placeholder' => '', 'label' => 'Jour et heure du rendez-vous', 'type' => 'datetime-local', 'value'=> $date->getDateHour()],
];

if (isset($_POST['modifDate']) && isset($_GET['dateId'])) {
    $errorList = [];
    $formVerif = new DateForm;

    $valueArray = [];
    foreach ($inputArray as $input) {
        if ($formVerif->checkPost($input)) {
            $valueArray[$input['name']] = $_POST[$input['name']];
        } else {
            $errorList[$input['name']] = $formVerif->getErrorMessage();
        }
    }
    if (count($errorList) == 0) {
        $date = new Date;
        $date->setDateId($_GET['dateId']);
        $date->setIdPatients(htmlspecialchars($valueArray['listPatient']));
        $date->setDateHour(htmlspecialchars($valueArray['dateTime']));
        if (!$date->checkHourIfExists()) {
            $date->modifDate();
            //include 'parts/toats.php';
            header('location: index.php');
            exit;
        }else{
            $errorList['addDate'] = 'Ce patient a déjà un rendez-vous!';
        }
    }

    }


