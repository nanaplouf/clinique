<?php
require 'class/Form.php';
require 'models/Patients.php';
$pageTitle = 'Profile';
$scriptList = ['assets/js/formPatient.js'];

$isPatientFound = false;
if (isset($_GET['patientId'])) {
    $patient = new Patients;
    $patient->setId(htmlspecialchars($_GET['patientId']));
    $isPatientFound = $patient->getPatientInfo();
    $patientDates = $patient->getPatientDates();
}
$inputArray = [
    ['filter' => 'name', 'name' => 'firstname', 'realName' => 'un prénom', 'placeholder' => '', 'label' => 'Prénom', 'type' => 'text', 'value'=> $patient->getFirstname()],
    ['filter' => 'name', 'name' => 'lastname', 'realName' => 'un nom de famille', 'placeholder' => '', 'label' => 'Nom de famille', 'type' => 'text', 'value'=> $patient->getLastname()],
    ['filter' => 'date', 'name' => 'birthdate', 'realName' => 'une date de naissance', 'placeholder' => '', 'label' => 'Date de naissance', 'type' => 'date', 'value'=> $patient->getBirthdateView()],
    ['filter' => 'phone', 'name' => 'phone', 'realName' => 'un numéro de téléphone', 'placeholder' => '', 'label' => 'Téléphone', 'type' => 'text', 'value'=> $patient->getPhone()],
    ['filter' => 'email', 'name' => 'mail', 'realName' => 'une adresse de couriel', 'placeholder' => '', 'label' => 'Adresse de courriel', 'type' => 'email', 'value'=> $patient->getMail()],
];

//Quand l'utilisateur a appuyé sur le bouton
if (isset($_POST['modifPatient'])) {
    $errorList = [];
    $formVerif = new Form;

    $valueArray = [];
    foreach ($inputArray  as $input) {
        if ($formVerif->checkPost($input)) {
            $valueArray[$input['name']] = $_POST[$input['name']];
        } else {
            $errorList[$input['name']] = $formVerif->getErrorMessage();
        }
    }

    if (count($errorList) == 0) {
        $patient->setLastname(htmlspecialchars($valueArray['lastname']));
        $patient->setFirstname(htmlspecialchars($valueArray['firstname']));
        $patient->setBirthdate(htmlspecialchars($valueArray['birthdate']));
        $patient->setPhone(htmlspecialchars($valueArray['phone']));
        $patient->setMail(htmlspecialchars($valueArray['mail']));
        if (!$patient->checkPatientIfExists()) {
            $patient->modifPatient();
            header('location: index.php');
            exit;
        }else{
            $errorList['addPatient'] = 'Ce patient existe déjà';
        }
    }
}

