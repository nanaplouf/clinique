<?php
$pageTitle = 'Liste des patients de notre hôpital';
$scriptList = ['assets/js/filterPatients.js'];
//Dans le cas de l'AJAX
if (isset($_POST['search'])) {
    require '../models/Patients.php';
    $patient = new Patients;
    echo json_encode($patient->getPatientListSelect($_POST['search']));
} else { //Dans les autres cas
    require 'models/Patients.php';
$patients = new Patients;
$patientList = $patients->getPatientListSelect();
$patientsList = $patients->getPatientsList();
if (isset($_POST['inputDelete'])) 
{
    $patients->setId($_POST['idDelete']);
//    $patients->deletePatient();
    header('location: index.php');
}
if(isset($_GET['page']) && !empty($_GET['page'])){
    $currentPage = (int) strip_tags($_GET['page']);
}else{
    $currentPage = 1;
}
$patients = new Patients;
$pagePatient= $patients->countPatient();
// On détermine le nombre d'articles par page
$parPage = 2;
// On détermine la première ligne à récupérer
$first = $currentPage * $parPage - $parPage;
$patientList = $patients->pagination($first, $parPage, $_POST['search'] ?? null);
// On calcule le nombre de pages total
$pages = ceil($pagePatient->nb_articles / $parPage);
}
