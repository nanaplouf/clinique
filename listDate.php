<?php include 'parts/head.php'; 
include 'controllers/listDateCtrl.php';
?>
<!--liste des rendez-vous. Inclure dans la page, un lien vers la création de rendez-vous
inclure la page date.php afficher les informations du rendez-vous sélectionné
permettre la suppression d'un rendez-vous-->
<h1 class="text-center">Liste des rendez-vous</h1>
 <table class="table table-striped mt-5 mb-5 p-5 tableNana">
   <thead>
     <tr>
       <th>Nom : </th>
       <th>Prénom : </th>
       <th>Date du rendez-vous : </th>
       <th>Heure : </th>
       <th>Voir plus : </th>
       <th>Supprimer le rendez-vous : </th>
     </tr>
   </thead>
   <tbody>
   <?php foreach($datesList as $date){ ?>
     <tr class="p-2">
       <td><?= $date->lastname;?></td>
       <td><?= $date->firstname;?></td>
       <td><?= $date->date;?></td>
       <td><?= $date->hour;?></td>
       <td><a href="date.php?dateId=<?= $date->dateId?>" class="aList"> Voir le rendez-vous</a></td>
      <td><form id="formDelete" action="" method="POST">
      <i class="fas fa-trash-alt"></i>
      <input type="hidden" id="idDelete" name="idDelete" value="<?= $date->dateId ?>">
      <input class="btn btn-dark" type="submit" value="Supprimer" name="inputDelete">
      </form></td>     
      </tr>
     <?php } ?>
   </tbody>
 </table>
 <a href="/addDate.php" class="aNana m-5 text-center mx-auto d-block col-2">Rajouter un rendez-vous</a>

<?php include 'parts/footer.php';?>