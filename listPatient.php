<?php include 'parts/head.php'; 
include 'controllers/listPatientCtrl.php'?>
<h1 class="text-center">Liste des patients</h1>
  <form method="POST">
    <div class="mb-3">
        <label for="searchPatient" class="form-label">Recherche</label>
        <input type="search" class="form-control" id="search" name="searchPatient" placeholder="Saisissez votre recherche">
    </div>
    <table class="table table-striped mt-5 mb-5 p-5 tableNana">
    <thead>
     <tr>
       <th>Nom : </th>
       <th>Prénom : </th>
       <th>Date de naissance : </th>
       <th>Profil</th>
       <th>Supprimer</th>
     </tr>
   </thead>
    <tbody for="patient" id="patient" name="patient" >
   <?php foreach($patientList as $patient){ ?>
     <tr class="p-2">
       <td><?= $patient->lastname ?></td>
       <td><?= $patient->firstname?></td>
       <td><?= $patient->birthdate?></td>
       <td><a href="profilePatient.php?patientId=<?= $patient->id;?>" class="aList"><i class="fas fa-user"></i> Voir le Profil</a></td>
       <td>
          <form id="formDelete" action="" method="POST">
            <i class="fas fa-trash-alt"></i>
            <input type="hidden" id="idDelete" name="idDelete" value="<?= $patient->id ?>">
            <input class="btn btn-dark" type="submit" value="Supprimer" name="inputDelete">
            </form></td>
      </tr>
      <?php } ?>
   </tbody>
 </table>
</form>
<ul class="pagination">
        <!-- Lien vers la page précédente (désactivé si on se trouve sur la 1ère page) -->
        <li class="page-item <?= ($currentPage == 1) ? "disabled" : "" ?>">
            <a href="./listPatient.php?page=<?= $currentPage - 1 ?>" class="page-link">Précédente</a>
        </li>
        <?php for($page = 1; $page <= $pages; $page++): ?>
            <!-- Lien vers chacune des pages (activé si on se trouve sur la page correspondante) -->
            <li class="page-item <?= ($currentPage == $page) ? "active" : "" ?>">
                <a href="./listPatient.php?page=<?= $page ?>" class="page-link"><?= $page ?></a>
            </li>
        <?php endfor ?>
            <!-- Lien vers la page suivante (désactivé si on se trouve sur la dernière page) -->
            <li class="page-item <?= ($currentPage == $pages) ? "disabled" : "" ?>">
            <a href="./listPatient.php?page=<?= $currentPage + 1 ?>" class="page-link">Suivante</a>
        </li>
    </ul>
 <a href="/addPatient.php" class="aNana m-5 text-center mx-auto d-block col-2">Rajouter un patient</a>
<?php include 'parts/footer.php';?>