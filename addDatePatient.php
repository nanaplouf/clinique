<?php 
include 'parts/head.php';
include 'controllers/datePatientCtrl.php';
?>
<!--créer un formulaire permettant de créer un rendez-vous-->
<h1 class="text-center">Ajout de rendez-vous</h1>
<form class="container formNana text-center" id="formDatePatient" action="" method="POST">
<?php foreach ($inputArray as $input) { ?>
        <label class="row col-2 pt-3 mx-auto" for="<?= $input['name']?>" class="form-label"><?= $input['label']?></label>
        <input class="col-2" type="<?= $input['type']?>" class="form-control" id="<?= $input['name']?>" name="<?= $input['name']?>" placeholder="<?= $input['placeholder']?>">
        <?php if (!empty($errorList[$input['name']])) { ?><p class="text-danger"><?= $errorList[$input['name']] ?></p><?php } 
         } ?>
        <input class="row col-2 mt-5 mx-auto btnNana" type="submit" value="Ajout du rendez-vous" name="validation">
        <?php if (!empty($errorList['addPatientDate'])) { ?>
                <p class="text-danger"><?= $errorList['addPatientDate']; ?></p>
        <?php } ?>
</form>
<?php include 'parts/footer.php';?>