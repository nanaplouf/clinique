<?php
class DateForm
{
    private string $inputValue;
    private string $regexId = '/^(\d)+$/';
    private string $regexDateTime = '/^(202[2-9])-((0[1-9])|(1[0-2]))-((0[1-9])|([1-2][0-9])|(3[0-1]))T(([01][0-9])|(2[0-3])):((00)|(15)|(30)|(45))$/';
    

    public function __construct()
    {
    }

    private function checkFormat(string $formatType): bool
    {
        switch ($formatType) {
            case 'listPatient':
                $check = preg_match($this->regexId, $this->inputValue);
                $this->errorMessage = 'Merci de renseigner ' . $this->inputNameError . ' ne contenant que des lettres, commençant par une majuscule et des séparateurs (espace, tiret).';
                break;
            case 'dateTime':
                $check = preg_match($this->regexDateTime, $this->inputValue);
                $this->errorMessage = 'Merci de renseigner ' . $this->inputNameError . ' ne contenant que des chiffres et des séparateurs (espace, tiret).';
                break;
            default:
                $check = false;
                break;
        }

        return $check;
    }
    private function isNotEmpty(): bool
    {
        $check = true;
        if (empty($this->inputValue)) {
            $this->errorMessage = 'Ce champ ne peut pas être vide.';
            $check = false;
        }
        return $check;
    }
    private function check(array $input, array $form): bool
    {
        $this->inputName = $input['filter'];
        $this->inputNameError = $input['realName'];
        $this->inputValue = $form[$input['name']];
        $check = false;
        $check = $this->isNotEmpty() && $this->checkFormat($this->inputName);
        return $check;
    }
    public function checkPost(array $input):bool{
        return $this->check($input, $_POST);
    }

    public function checkGet(array $input):bool{
        return $this->check($input, $_GET);
    }

    public function getErrorMessage(): string
    {
        return $this->errorMessage;
    }
}
