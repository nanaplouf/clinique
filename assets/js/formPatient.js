let clickInfo = document.getElementById("clickInfo");
let infoPatient = document.getElementById("infoPatient");
let formPatient = document.getElementById("formPatient");
if (clickInfo != undefined){
    clickInfo.addEventListener("click", () => {
    if(getComputedStyle(infoPatient).display != "none"){
        infoPatient.style.display = "none";
        formPatient.style.display = "block";
    } else {
        infoPatient.style.display = "block";
        formPatient.style.display = "none";
    }
  });
}
