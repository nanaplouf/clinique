let clickInfoDate = document.getElementById("clickInfoDate");
let infoDate = document.getElementById("infoDate");
let formDateModif = document.getElementById("formDateModif");
if (clickInfoDate != undefined){
    clickInfoDate.addEventListener("click", () => {
    if(getComputedStyle(infoDate).display != "none"){
        infoDate.style.display = "none";
        formDateModif.style.display = "block";
    } else {
        infoDate.style.display = "block";
        formDateModif.style.display = "none";
    }
  });
}