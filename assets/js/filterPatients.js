search.addEventListener("input",function(){
    let value = this.value
    //On instancie l'objet XMLHttpRequest pour faire de l'AJAX
    let xhr = new XMLHttpRequest()
    //On ouvre une connexion en post vers le controller
    xhr.open("POST", "controllers/listPatientCtrl.php", true)
    //On oublie pas ça sinon le controller ne recevera rien
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded")
    //On spécifie les données à envoyer
    xhr.send("search="  + value) // $_POST['search'] = value
    //On attend une réponse du controller
    xhr.onreadystatechange = function () {
        //Si on a reçu une réponse et qu'elle est positive
        if (xhr.readyState == 4 && xhr.status == 200) {
            //On récupère un json
            const jsonPatient = xhr.responseText
            //On le parse pour pouvoir le parcourir
            const jsonPatientArray = JSON.parse(jsonPatient)
            //On vide la liste déroulante            
            patient.innerText = ""
            //On parcours le JSON
            jsonPatientArray.map((patientSelect)=>{
                //On selectionne la liste déroulante
                patient = document.getElementById("patient")
                //On crée une balise option virtuel
                let tr = document.createElement("tr")
                let lastname = document.createElement("td")
                let firstname = document.createElement("td")
                let birthdate = document.createElement("td")
                let idTd = document.createElement("td")
                let idButton = document.createElement("a")
                idButton.href = "profilePatient.php?patientId=" + patientSelect.id
                let inputTd = document.createElement("td")
                let inputForm = document.createElement("form")
                inputForm.method = "POST"
                inputForm.id = "formDelete"
                let inputOne = document.createElement("input")
                inputOne.type = "hidden"
                inputOne.id = "idDelete"
                inputOne.name = "idDelete"
                inputOne.value = patientSelect.id
                let inputTwo = document.createElement("input")
                inputTwo.type = "submit"
                inputTwo.className = "btn btn-dark"
                inputTwo.name = "inputDelete"
                inputTwo.value = "Supprimer"


                //On lui donne un nom
                lastname.innerText = patientSelect.lastname
                firstname.innerText = patientSelect.firstname
                birthdate.innerText = patientSelect.birthdate
                idButton.innerText = "Voir plus"
        
                //On l'attache au tableau
                inputForm.append(inputOne, inputTwo)
                inputTd.append(inputForm)
                idTd.appendChild(idButton)
                tr.append(lastname, firstname, birthdate, idTd, inputTd)
                patient.appendChild(tr)

            })
        }
    };
})