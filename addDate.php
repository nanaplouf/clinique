<?php 
include 'parts/head.php';
include 'controllers/listPatientCtrl.php';
include 'controllers/addDateCtrl.php';
?>
<!--créer un formulaire permettant de créer un rendez-vous-->
<h1 class="text-center">Ajout de rendez-vous</h1>
<form class="container formNana text-center" id="formDate" action="" method="POST">
        <label class="row col-2 pt-3 mx-auto" for="listPatient" class="form-label">Recherche nom du patient : </label>
        <select class="row col-2 mx-auto" list="patient" id="listPatient" name="listPatient">
        <?php foreach($patientsList as $patient){ ?>
                <option class="optionDatalist" value="<?= $patient->id ?>">
                        <?= $patient->lastname . ' ' . $patient->firstname . ' - ' . $patient->birthdate ?>
                </option>
        <?php } ?>
        </select>
        <?php if (!empty($errorList['listPatient'])) { ?>
                <p class="text-danger"><?= $errorList['listPatient']; ?></p>
        <?php } ?>
        <label class="row col-2 pt-3 mx-auto" for="dateTime" class="form-label">Jour et heure du rendez-vous : </label>
        <input class="col-2" type="datetime-local" name="dateTime">
        <?php if (!empty($errorList['dateTime'])) { ?>
                <p class="text-danger"><?= $errorList['dateTime']; ?></p>
        <?php } ?>
        <input class="row col-2 mt-5 mx-auto btnNana" type="submit" value="Ajout du rendez-vous" name="validation">
        <?php if (!empty($errorList['addDate'])) { ?>
                <p class="text-danger"><?= $errorList['addDate']; ?></p>
        <?php } ?>
</form>
<?php include 'parts/footer.php';?>