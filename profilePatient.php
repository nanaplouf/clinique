<!--Elle doit permettre d'afficher toutes les informations d'un patient
permettre la modification de ce patient
afficher sous ses informations la liste de ses rendez-vous-->
<?php 
include 'parts/head.php'; 
include 'controllers/patientCtrl.php';
?>
<div id="infoPatient">
<?php if ($isPatientFound) { ?>
    <h1 class="text-center">Information du patient <?= $patient->getLastname() ?> <?= $patient->getFirstname() ?></h1>
    <div class="container formNana text-center">
        <div class="row">
            <div class="col"><p class="fs-4">Nom : </p></div>
            <div class="col"><p class="pNana"><?= $patient->getLastname() ?></p></div>
        </div>
        <div class="row">
            <div class="col"><p class="fs-4">Prénom: </p></div>
            <div class="col"><p class="pNana"><?= $patient->getFirstname()?></p></div>
        </div>
        <div class="row">
            <div class="col"><p class="fs-4">Date de naissance : </p></div>
            <div class="col"><p class="pNana"><?= $patient->getBirthdate()?></p></div>
        </div>
        <div class="row">
            <div class="col"><p class="fs-4">Numéro de téléphone :</p></div>
            <div class="col"><a class="aMailPhone" href="mailto:<?= $patient->getMail() ?>"><?= $patient->getMail() ?></a></div>
        </div>
        <div class="row">
            <div class="col"><p class="fs-4">Adresse de courriel :</p></div>
            <div class="col"><a class="aMailPhone" href="telto:<?= $patient->getPhone() ?>"><?= $patient->getPhone() ?></a></div>
        </div>
    </div>
    <button id="clickInfo" class="aNana m-5 text-center mx-auto d-block col-2">Modifier les informations</button>
</div>
<div id="formPatient">
<h1 class="text-center">Modifier les informations</h1>
    <form class="container formNana text-center" action="" method="POST">
    <?php foreach ($inputArray as $input) { ?>
        <label class="row col-2 pt-3 mx-auto" for="<?= $input['name']?>" class="form-label"><?= $input['label']?></label>
        <input class="col-2" type="<?= $input['type']?>" class="form-control" id="<?= $input['name']?>" name="<?= $input['name']?>" value="<?= $input['value']?>" placeholder="<?= $input['placeholder']?>">
        <?php if (!empty($errorList[$input['name']])) { ?><p class="text-danger"><?= $errorList[$input['name']] ?></p><?php } 
         } ?>
        <input class="row col-2 mt-5 mx-auto btnNana" type="submit" name="Patient" value="Modifier patient"name="validation" value="Enregistrer">
    </form>
</div>
<h2 class="text-center">Rendez-vous de <?= $patient->getLastname() ?> <?= $patient->getFirstname() ?></h2>
<table class="table table-striped mt-5 mb-5 p-5 tableNana">
   <thead>
     <tr>
       <th>Date du rendez-vous : </th>
       <th>Heure : </th>
     </tr>
   </thead>
   <tbody>
   <?php foreach($patientDates as $patient){ ?>
     <tr class="p-2">
       <td><?= $patient->date;?></td>
       <td><?= $patient->hour;?></td>
     </tr>
     <?php }?>
   </tbody>
   <tbody>
 </table>
 <?php } else { ?>
    <h1 class="text-center">Patient non trouvé</h1>
    <p class="text-danger text-center fs-4">Merci de contacter le service technique si le problème persiste!</p>
    <a class="aNana row col-1 m-5 text-center mx-auto d-block" href="liste-patients.php" class="btn btn-primary">Retour</a>
<?php } ?>
<?php include 'parts/footer.php';?>